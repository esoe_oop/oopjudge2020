/**
 * The class SentenceProcessor provides two methods which can remove duplicated words and replace word.
 */
public class SentenceProcessor {
    /**
     * @param sin is the string (sentence) inputted, and it may have some duplicated words.
     * @return the sentence that the duplicated had been removed. The first word will be persevered. The words in upper case and in lower case are different.
     */
    public String removeDuplicatedWords(String sin){
        String[] ss=sin.split("\\s+");
        StringBuffer sbb=new StringBuffer();
        for(int i=0;i<ss.length;i++){
            for(int j=0;j<i;j++){
                if(ss[i].equals(ss[j])){
                      ss[i]="";
                }
            }
            sbb.append(" ").append(ss[i]);
        }
        String s=sbb.toString();
        String[] sq=s.split("\\s+");
        StringBuffer sb3=new StringBuffer();
        for(int t=0;t<sq.length;t++){
            if("".equals(sq[t])){
                continue;
            }
            sb3.append(sq[t]);
            if(t != sq.length - 1) {
                sb3.append(" ");
            }
        }
        String sp = sb3.toString();
        return sp;
    }

    /**
     * @param s1 is the word in the origin sentence that is going to be replaced.
     * @param s2 is the new word that is going to replace s1.
     * @param s_total is the total sentence inputted.
     * @return the sentence after removal.
     */
    public String replaceWord(String s1, String s2, String s_total){
        String[] s_total_part = s_total.split("\\s+");
        StringBuffer s_buff = new StringBuffer();
        for(int i=0;i<s_total_part.length;i++){
            for(int j=0;j<i;j++){
                if(s_total_part[i].equals(s1)){
                      s_total_part[i] = s2;
                }
            }
            s_buff.append(s_total_part[i]);
            if(i != s_total_part.length - 1) {
                s_buff.append(" ");
            }
        }
        String s_re =s_buff.toString();
        return s_re;
    }
}
